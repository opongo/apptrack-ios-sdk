//
//  main.m
//  AppTrackSDKExample
//
//  Created by Tuan Huy Tran on 15.05.14.
//  Copyright (c) 2014 Tuan Huy Tran. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "OPAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([OPAppDelegate class]));
    }
}
