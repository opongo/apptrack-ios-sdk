//
//  OPAppDelegate.h
//  AppTrackSDKExample
//
//  Created by Tuan Huy Tran on 15.05.14.
//  Copyright (c) 2014 Tuan Huy Tran. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
