//
//  OPAppTrack.m
//  dealbunny
//
//  Created by Tuan Huy Tran on 15.05.14.
//  Copyright (c) 2014 Huy Tran. All rights reserved.
//

#import "OPAppTrack.h"
#import "OPHttpRequest.h"
#import "UIDevice+OPCategories.h"
#import "NSString+OPCategories.h"

static NSString   * const kOPServerURL = @"https://analytics.opongo.com/api";
static NSString   * const kOPDebugServerURL = @"https://analytics.opongo.com/api/debug";
static float        const kOPStartDelay = 1.5;

static OPAppTrack *apptrackSingleton = nil;

@interface OPAppTrack()

@property (nonatomic, retain) OPHttpRequest *httpRequest;

@end


@implementation OPAppTrack

@synthesize httpRequest = _httpRequest;
@synthesize debug = _debug;
@synthesize token = _token;

+ (OPAppTrack *)apptrackSingleton {
	@synchronized(self)
	{
		if (!apptrackSingleton) {
            apptrackSingleton = [[OPAppTrack alloc] init];
            
            [[NSNotificationCenter defaultCenter] addObserver:apptrackSingleton selector:@selector(startSessionWithDelay) name:UIApplicationWillEnterForegroundNotification object:nil];
        }
	}
    
	return apptrackSingleton;
}

#pragma mark - Sessions
- (void)startSessionWithToken:(NSString*)customerToken {
    self.token = customerToken;
    [self startSessionWithDelay:kOPStartDelay];
}

- (void)startSessionWithDelay {
    [self startSessionWithDelay:kOPStartDelay];
}

- (void)startSessionWithDelay:(float)delay {
    [self performSelector:@selector(startSession) withObject:nil afterDelay:delay];
}

- (void)startSession {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSDictionary *jsonResults = [defaults objectForKey:@"OPA_JSON_RESULTS"];
    int delay = 0;
    
    if ([jsonResults objectForKey:@"delay"]) {
         delay = [[jsonResults objectForKey:@"delay"] intValue];
    }
    
    NSDate *lastSessionDate = [defaults objectForKey:@"OPA_LAST_SESSION_DATE"];
    
    if (delay == 0 || lastSessionDate == nil || [lastSessionDate timeIntervalSinceNow] < (delay*-1)) {
        [OPAppTrack log:@"Server-URL: %@", self.debug?kOPDebugServerURL:kOPServerURL];
        [OPAppTrack log:@"LastSessionDate: %@", [NSNumber numberWithInt:[lastSessionDate timeIntervalSinceNow]]];
        
        NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
        
        NSDictionary *query = [NSDictionary dictionaryWithObjectsAndKeys:
                               self.token, @"token",
                               [UIDevice currentDevice].opAdvertisingIdentifier, @"advertising_identifier",
                               [UIDevice currentDevice].opDeviceName, @"device_name",
                               [UIDevice currentDevice].opDeviceType, @"device_type",
                               language, @"language",
                               nil];
        
        [self.httpRequest sendRequest:query completed:^(NSData *data) {
             NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            if (![dataString isEqualToString:@""]) {
                NSError *error = nil;
                NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                
                if(error) {
                    [OPAppTrack log:@"Error parsing JSON Data: %@", error];
                    [OPAppTrack log:@"Response: %@", dataString];
                    return;
                }
                
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setObject:JSON forKey:@"OPA_JSON_RESULTS"];
            }
        } failed:^(NSError *error) {
            
        }];
        
        [defaults setObject:[NSDate date] forKey:@"OPA_LAST_SESSION_DATE"];
    }

}

#pragma mark - Debug
+ (BOOL)debug {
    return [OPAppTrack apptrackSingleton].debug;
}

#pragma mark - Log
+ (void)log:(id)firstObject, ...  {
    if ([OPAppTrack debug]) {
        
        va_list argumentList;
        va_start(argumentList, firstObject);
        
        NSLogv(firstObject, argumentList);
        
        va_end(argumentList);
    }
}

#pragma mark - init properties
- (OPHttpRequest*)httpRequest {
    if (_httpRequest == nil) {
        NSString *url = self.debug?kOPDebugServerURL:kOPServerURL;
        _httpRequest = [[OPHttpRequest alloc] initWithHost:url];
    }
    
    return _httpRequest;
}

@end
