//
//  OPHttpRequest.h
//  dealbunny
//
//  Created by Tuan Huy Tran on 15.05.14.
//  Copyright (c) 2014 Huy Tran. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OPHttpRequest : NSObject

- (id)initWithHost:(NSString*)host;
- (void)sendRequest:(NSDictionary*)query;
-(void)sendRequest:(NSDictionary *)query completed:(void(^)(NSData *data))completedBlock failed:(void(^)(NSError *error))failedBlock;

- (void)empty;

@end
