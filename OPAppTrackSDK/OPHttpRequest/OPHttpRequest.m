//
//  OPHttpRequest.m
//  dealbunny
//
//  Created by Tuan Huy Tran on 15.05.14.
//  Copyright (c) 2014 Huy Tran. All rights reserved.
//

#import "OPHttpRequest.h"
#import "NSDictionary+OPCategories.h"
#import "OPAppTrack.h"

@interface OPHttpRequest() {
    int retry;
}

@property (nonatomic, retain) NSOperationQueue *queue;
@property (nonatomic, retain) NSString *url;

@end


@implementation OPHttpRequest

@synthesize queue = _queue;

-(id)initWithHost:(NSString*)url {
    self = [super init];
    
    if (self == nil) return nil;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(empty) name:UIApplicationWillResignActiveNotification object:nil];
    
    self.url = url;
    
    return self;
}

- (void)sendRequest:(NSDictionary*)query {
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
        [self.queue addOperationWithBlock:^{
            [self sendRequestWithUrl:self.url andQuery:query];
        }];
    }
}

-(void)sendRequest:(NSDictionary *)query completed:(void(^)(NSData *data))completedBlock failed:(void(^)(NSError *error))failedBlock {
    dispatch_queue_t main = dispatch_get_main_queue();
    
    NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^{
        id data = [self sendRequestWithUrl:self.url andQuery:query];
        
        dispatch_async(main,^{
            if ([data isKindOfClass:[NSData class]]) {
                completedBlock(data);
            } else {
                failedBlock(data);
            }
        });
    }];
    
    [self.queue addOperation:operation];
}

- (NSOperationQueue*)queue {
    if (_queue == nil) {
        _queue = [[NSOperationQueue alloc] init];
        [_queue setMaxConcurrentOperationCount:1];
    }
    
    return _queue;
}

- (id) sendRequestWithUrl:(NSString*)urlString andQuery:(NSDictionary*)query {
    NSString *server_url = urlString;
    
    NSURL *url = [NSURL URLWithString:server_url];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:10.0];
    
    NSData *postData = [query.queryString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
	[request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
	[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
	[request setValue:@"gzip" forHTTPHeaderField:@"Accept-Encoding"];
	[request setHTTPBody:postData];
    
	NSURLResponse *response = nil;
    NSError *error = nil;
    
	NSData *urlData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    if (error) {
        return error;
    }
    
    return urlData;
}


- (void)sendRetryAfterDelay:(NSNotification*) notification {
    [OPAppTrack log:@"Retry send after 10 sec"];
    if (retry < 3) {
        retry++;
        [self performSelector:@selector(sendRequest:) withObject:notification.userInfo afterDelay:10.0];
    }
  
}

- (void)empty{
    [OPAppTrack log:@"Empty queue"];
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self.queue cancelAllOperations];
}

-(void)dealloc {
    [self empty];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
