//
//  OPAppTrack.h
//  dealbunny
//
//  Created by Tuan Huy Tran on 15.05.14.
//  Copyright (c) 2014 Huy Tran. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OPAppTrack : NSObject

+ (OPAppTrack *)apptrackSingleton;
+ (BOOL)debug;

- (void) startSessionWithToken:(NSString*)customerToken;
- (void) startSessionWithDelay:(float)delay;
- (void) startSessionWithDelay;
- (void) startSession;
+ (void)log:(id)firstObject, ... ;

@property (nonatomic, retain) NSString *token;
@property (assign) bool debug;

@end
