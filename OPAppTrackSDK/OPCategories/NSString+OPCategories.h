//
//  NSString+OPCategories.h
//  dealbunny
//
//  Created by Tuan Huy Tran on 15.05.14.
//  Copyright (c) 2014 Huy Tran. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (OPCategories)

- (NSString *)opSha1;
- (NSString *)opMd5;
- (NSString *)opUrlEncode;
- (NSString *)escapeString;

@end
