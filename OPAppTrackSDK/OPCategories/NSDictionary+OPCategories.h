//
//  NSDictionary+OPCategories.h
//  dealbunny
//
//  Created by Tuan Huy Tran on 15.05.14.
//  Copyright (c) 2014 Huy Tran. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (OPCategories)

- (NSString *)queryString;
- (NSString *)jsonString;

@end
