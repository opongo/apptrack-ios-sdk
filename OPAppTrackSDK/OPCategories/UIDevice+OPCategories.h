//
//  UIDevice+OPCategories.h
//  dealbunny
//
//  Created by Tuan Huy Tran on 15.05.14.
//  Copyright (c) 2014 Huy Tran. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIDevice (OPCategories)

- (BOOL)opAdvertisingTrackingEnabled;
- (NSString *)opAdvertisingIdentifier;
- (NSString *)opMacAddress;
- (NSString *)opDeviceType;
- (NSString *)opDeviceName;

@end
