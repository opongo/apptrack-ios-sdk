//
//  NSDictionary+OPCategories.m
//  dealbunny
//
//  Created by Tuan Huy Tran on 15.05.14.
//  Copyright (c) 2014 Huy Tran. All rights reserved.
//

#import "NSDictionary+OPCategories.h"
#import "NSString+OPCategories.h"

@implementation NSDictionary (OPCategories)

- (NSString *)queryString
{
    NSMutableString *queryString = [[NSMutableString alloc] init];;
    NSArray *keys = [self allKeys];
    
    if ([keys count] > 0) {
        for (NSString *key in keys) {
            id value = [self objectForKey:key];
            
            if ([value isKindOfClass:[NSString class]]) {
                NSString *valueString = value;
                value = valueString.escapeString;
            }
            
            if (![queryString isEqualToString:@""]) {
                [queryString appendFormat:@"&"];
            }
            
            if (nil != key && nil != value) {
                [queryString appendFormat:@"%@=%@", key.escapeString, value];
            } else if (nil != key) {
                [queryString appendFormat:@"%@", key.escapeString];
            }
        }
    }
    
    return queryString;
}

- (NSString *)jsonString
{
    NSError *error = nil;
    NSData *json;
    
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:self])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:self options:NSJSONWritingPrettyPrinted error:&error];
        
        // If no errors, let's view the JSON
        if (json != nil && error == nil)
        {
            return [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
        }
    }
    
    return nil;
}

@end
