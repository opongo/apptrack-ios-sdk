//
//  NSString+OPCategories.m
//  dealbunny
//
//  Created by Tuan Huy Tran on 15.05.14.
//  Copyright (c) 2014 Huy Tran. All rights reserved.
//

#import "NSString+OPCategories.h"

#import "CommonCrypto/CommonDigest.h"

@implementation NSString (OPCategories)

- (NSString *)opSha1 {
    const char *cstr = [self cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:self.length];
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    CC_SHA1(data.bytes, (CC_LONG)data.length, digest);
    
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++) {
        [output appendFormat:@"%02x", digest[i]];
    }
    return output;
}

- (NSString *)opMd5 {
    const char *cStr = [self UTF8String];
    unsigned char digest[16];
    CC_MD5(cStr, (CC_LONG)strlen(cStr), digest);
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        [output appendFormat:@"%02x", digest[i]];
    }
    return  output;
}

-(NSString *)opUrlEncode {
    return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
             NULL,
             (CFStringRef)self,
             NULL,
             (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ",
             CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding))
    );
}

-(NSString *) escapeString {
    NSString *s = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                                        (CFStringRef)self,
                                                                                        NULL,
                                                                                        (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ",
                                                                                        kCFStringEncodingUTF8));
    return s;
}

@end
